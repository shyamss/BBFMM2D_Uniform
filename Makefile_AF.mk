CC			=g++-6
CFLAGS		=-c -Wall -Ofast -fopenmp -ffast-math -ffinite-math-only -std=c++11
LDFLAGS		=-Ofast -fopenmp
LIBS        = -laf
SOURCES		=./testFMM2D_AF.cpp ./FMM2DTree_AF.hpp
KERNEL		=-DONEOVERR	# use -DONEOVERR, -DLOGR
HOMOG		=-DHOMOG	# use -DHOMOG, -DLOGHOMOG
OBJECTS		=$(SOURCES:.cpp=.o)
EXECUTABLE	=./testFMM2D_AF

# Single Dynamic Library
$(EXECUTABLE): $(OBJECTS)
		$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@

.cpp.o:
		$(CC) $(CFLAGS) $(KERNEL) $(HOMOG) $(LIBS) $< -o $@

clean:
	rm testFMM2D_AF *.o
