CC			=g++-6
CFLAGS		=-c -Wall -Ofast -fopenmp -ffast-math -ffinite-math-only
LDFLAGS		=-Ofast -fopenmp
SOURCES		=./testFMM2D_Eigen.cpp ./FMM2DTree_Eigen.hpp
KERNEL		=-DONEOVERR	# use -DONEOVERR, -DLOGR
HOMOG		=-DHOMOG	# use -DHOMOG, -DLOGHOMOG
OBJECTS		=$(SOURCES:.cpp=.o)
EXECUTABLE	=./testFMM2D_Eigen

all: $(SOURCES) $(EXECUTABLE)

# Static Linking:
#$(EXECUTABLE): $(OBJECTS)
#		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH)  -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread -lm -ldl $(LDFLAGS) $(OBJECTS) -o $@

#.cpp.o:
#		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH)  -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread -lm -ldl $(CFLAGS) $(KERNEL) $(HOMOG) $< -o $@

# Dynamic Linking:
# $(EXECUTABLE): $(OBJECTS)
# 		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH) -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl $(LDFLAGS) $(OBJECTS) -o $@

# .cpp.o:
# 		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH) -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm  $(CFLAGS) $(KERNEL) $(HOMOG) $< -o $@

# Single Dynamic Library
$(EXECUTABLE): $(OBJECTS)
		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH) -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_rt -lpthread -lm -ldl $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
		$(CC) -m64 -I${MKLROOT}/include -I $(EIGEN_PATH) -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_rt -lpthread -lm -ldl $(CFLAGS) $(KERNEL) $(HOMOG) $< -o $@

clean:
	rm testFMM2D_Eigen *.o
