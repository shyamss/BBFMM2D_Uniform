CC			=g++-6
CFLAGS		=-c -Wall -Ofast -fopenmp -ffast-math -ffinite-math-only
LDFLAGS		=-Ofast -fopenmp
SOURCES		=./testFMM2D_Eigen_AF.cpp ./FMM2DTree_Eigen_AF.hpp
KERNEL		=-DONEOVERR
HOMOG		=-DHOMOG
OBJECTS		=$(SOURCES:.cpp=.o)
EXECUTABLE	=./testFMM2D_Eigen_AF

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
		$(CC) -I $(EIGEN_PATH) $(LDFLAGS) $(OBJECTS) -lafcuda -o $@

.cpp.o:
		$(CC) -I $(EIGEN_PATH) $(CFLAGS) $(KERNEL) $(HOMOG) -lafcuda $< -o $@

clean:
	rm testFMM2D_Eigen_AF *.o
