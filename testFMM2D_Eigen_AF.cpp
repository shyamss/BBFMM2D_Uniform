#include <iostream>
#include "FMM2DTree_Eigen_AF.hpp"

class userkernel: public kernel {
public:
    #ifdef ONEOVERR
    userkernel() {
        isTrans     =   true;
        isHomog     =   true;
        isLogHomog  =   false;
        alpha       =   -1.0;
    };
    double getInteraction(const pts2D r1, const pts2D r2, double a) {
        double R2   =   (r1.x-r2.x)*(r1.x-r2.x) + (r1.y-r2.y)*(r1.y-r2.y);
        double R    =   sqrt(R2);
        if (R < a) {
            return R/a;
        }
        else {
            return a/R;
        }
    };
    #elif LOGR
    userkernel() {
        isTrans     =   true;
        isHomog     =   false;
        isLogHomog  =   true;
        alpha       =   1.0;
    };
    double getInteraction(const pts2D r1, const pts2D r2, double a) {
        double R2   =   (r1.x-r2.x)*(r1.x-r2.x) + (r1.y-r2.y)*(r1.y-r2.y);
        if (R2 < 1e-10) {
            return 0.0;
        }
        else if (R2 < a*a) {
            return 0.5*R2*log(R2)/a/a;
        }
        else {
            return 0.5*log(R2);
        }
    };
    #endif
    ~userkernel() {};
};


int main(int argc, char* argv[]) {
    int nLevels     =   atoi(argv[1]);
    int nChebNodes  =   atoi(argv[2]);
    int L           =   atoi(argv[3]);

    double start, end;
    double timeAFPre, timeAFEval, timeAssemble, timeLeaf, 
           timeCreateTree, timeL2L, timeM2M, timeM2L, timeAssignCharges;
    int N; // Number of particles

    // We are first creating the tree:
    start = omp_get_wtime();
    
    userkernel* mykernel     = new userkernel();
    FMM2DTree<userkernel>* A = new FMM2DTree<userkernel>(mykernel, nLevels, nChebNodes, L);

    A->set_Standard_Cheb_Nodes();
    A->createTree();
    A->assign_Tree_Interactions();

    end            = omp_get_wtime();
    timeCreateTree = (end-start);

    N = A->N;

    start = omp_get_wtime();

    A->assemble_Operators_FMM();
    A->get_Transfer_Matrix();

    end          = omp_get_wtime();
    timeAssemble = (end-start);

    start = omp_get_wtime();

    A->assign_Center_Location();
    A->assign_Leaf_Charges();

    end               = omp_get_wtime();
    timeAssignCharges = (end-start);

    #pragma omp parallel
    {
        #pragma omp single nowait
        {
            start     = omp_get_wtime();
            A->AFGetLeafOperators();
            A->AFGetChargesAtLeafLevel();
            end       = omp_get_wtime();
            timeAFPre = (end-start);

            start      = omp_get_wtime();
            A->evaluate_LeafAF();
            end        = omp_get_wtime();
            timeAFEval = (end-start);
        }

        start   = omp_get_wtime();
        A->evaluate_All_M2M();
        end     = omp_get_wtime();
        timeM2M = (end-start);

        start   = omp_get_wtime();
        A->evaluate_All_M2L();
        end     = omp_get_wtime();
        timeM2L = (end-start);

        start   = omp_get_wtime();
        A->evaluate_All_L2L();
        end     = omp_get_wtime();
        timeL2L = (end-start);

        // start    =   omp_get_wtime();
        // A->evaluate_Leaf();
        // end      =   omp_get_wtime();
        // timeLeaf =   (end-start);
    }

    af::sync();

    std::cout << std::endl << "Number of particles is: " << N << std::endl;
    std::cout << std::endl << "Time taken to create the tree is: " << timeCreateTree << std::endl;
    std::cout << std::endl << "Time taken to assemble the operators is: " << timeAssemble << std::endl;
    std::cout << std::endl << "Time taken to assemble the charges is: " << timeAssignCharges << std::endl;
    std::cout << std::endl << "Time taken for multipole to multipole is: " << timeM2M << std::endl;
    std::cout << std::endl << "Time taken for multipole to local is: " << timeM2L << std::endl;
    std::cout << std::endl << "Time taken for local to local is: " << timeL2L << std::endl;
    std::cout << std::endl << "Time taken for self and neighbors at leaf is: " << timeLeaf << std::endl;
    std::cout << std::endl << "Time taken for AF processing is: " << timeAFPre << std::endl;
    std::cout << std::endl << "Time taken for AF leafeval is: " << timeAFEval << std::endl;

    // start    =   omp_get_wtime();
    // A->sendToHost();
    // end      =   omp_get_wtime();
    // double timeAFTransfer            =   (end-start);
    // std::cout << std::endl << "Time taken for AF transfer is: " << timeAFTransfer << std::endl;

    // double totalTime    =   timeCreateTree+timeAssemble+timeAssignCharges+timeAssemble+timeM2M+timeM2L+timeL2L+timeLeaf;
    
    // double applyTime    =   timeM2M+timeM2L+timeL2L+timeLeaf;

    // std::cout << std::endl << "Total time taken is: " << totalTime << std::endl;

    // std::cout << std::endl << "Apply time taken is: " << applyTime << std::endl;

    // std::cout << std::endl << "Total Speed in particles per second is: " << A->N/totalTime << std::endl;

    // std::cout << std::endl << "Apply Speed in particles per second is: " << A->N/applyTime << std::endl;

    // std::cout << std::endl << "Number of particles is: " << A->N << std::endl;

    // std::cout << std::endl << "Error Check Details:" << std::endl;

    //    srand(time(NULL));
    // int nBox =   rand()%A->nBoxesPerLevel[nLevels];
    // std::cout << std::endl << "Box number is: " << nBox << std::endl;
    // std::cout << std::endl << "Box center is: (" << A->tree[nLevels][nBox].center.x << ", " << A->tree[nLevels][nBox].center.y << ");" << std::endl;
    // start    =   omp_get_wtime();
    // std::cout << std::endl << "Error is: " << A->perform_Error_Check(nBox) << std::endl;
    // end      =   omp_get_wtime();
    // double errorTime =   (end-start);

    // std::cout << std::endl << "Time taken to compute error is: " << errorTime << std::endl;

    // std::cout << std::endl;
}
